//mpp recorder v1

// record
var recording = [];
MPP.client.on('n', msg => {
    recording.push(msg);
});

// playback
recording.forEach(msg => {
    setTimeout(()=>{
        msg.n.forEach(n => {MPP.press(n.n, n.v)});
    }, msg.t - recording[0].t)
});






//mpp recorder v2

//record
var recording = [];
MPP.client.on("n", function(msg) {
	var t = msg.t - MPP.client.serverTimeOffset + 1000 - Date.now();
	msg.n.forEach((note, i) => {
		var ms = t + (note.d || 0);
		if (ms < 0) ms = 0; else if (ms > 10000) return;
		if (note.s) {
			recording.push({tp:"r", ky:note.n, ts:Date.now()+ms}); // key releases
		} else {
			var vel = typeof note.v !== "undefined" ? parseFloat(note.v) : 0.5;
			if (vel < 0) vel = 0; else if (vel > 1) vel = 1;
			recording.push({tp:"p", ky:note.n, ts:Date.now()+ms, vl:vel}); // key presses
		}
	});
});


//playback
recording.forEach(note => {
	setTimeout(()=>{
		if (note.tp === "p") MPP.press(note.ky, note.vl);
		if (note.tp === "r") MPP.release(note.ky);
	}, note.ts - recording[0].ts);
});







// mpp recorder v3

//record
var recording = ";";
var startTime = Date.now();
MPP.client.on("n", msg => {
	var t = msg.t - MPP.client.serverTimeOffset + 1000 - Date.now();
	msg.n.forEach((note, i) => {
		var ms = t + (note.d || 0);
		if (ms < 0) ms = 0; else if (ms > 10000) return;
		if (note.s) {
			recording += `${Date.now()+ms-startTime},${note.n};`; // key releases
		} else {
			var vel = note.v ? parseFloat(note.v) : 0.5;
			if (vel < 0) vel = 0; else if (vel > 1) vel = 1;
			recording += `${Date.now()+ms-startTime},${note.n},${vel};`; // key presses
		}
	});
});

// playback
var timers = [];
recording.split(';').slice(1,-1).forEach(note => {
	var keys = note.split(',');
	timers.push(setTimeout(()=>{
		if (keys[2]) MPP.press(keys[1], Number(keys[2]));
		else MPP.release(keys[1]);
    }, keys[0]));
});


// mpp recorder v4/functionalized

// Basic note recorder for use in web console, made by Lamp
// To use, paste into console, then control with the following functions (type into console)
// record(), stopRecord(), playRecording(), stopPlayingRecording(), saveRecording("name"), loadRecording("name"), and resetRecording()

var recording = ";";
var isRecording = false;
var startTime;
var timers = [];
MPP.client.on("n", msg => {
	if (!isRecording) return;
	var t = msg.t - MPP.client.serverTimeOffset + 1000 - Date.now();
	msg.n.forEach((note, i) => {
		var ms = t + (note.d || 0);
		if (ms < 0) ms = 0; else if (ms > 10000) return;
		if (note.s) {
			recording += `${Date.now()+ms-startTime},${note.n};`; // key releases
		} else {
			var vel = note.v ? parseFloat(note.v) : 0.5;
			if (vel < 0) vel = 0; else if (vel > 1) vel = 1;
			recording += `${Date.now()+ms-startTime},${note.n},${vel};`; // key presses
		}
	});
});
function record() {
	startTime = Date.now();
	isRecording = true;
}
function stopRecord() {
	isRecording = false;
}
function playRecording() {
	recording.split(';').slice(1,-1).forEach(note => {
		var keys = note.split(',');
		timers.push(setTimeout(()=>{
			if (keys[2]) MPP.press(keys[1], Number(keys[2]));
			else MPP.release(keys[1]);
		}, keys[0]));
	});
}
function stopPlayingRecording() {
	timers.forEach(timer => {clearTimeout(timer)});
	timers = [];
}
function saveRecording(name) {
	localStorage[name] = recording;
}
function loadRecording(name) {
	recording = localStorage[name];
}
function resetRecording() {
	recording = ";";
}

